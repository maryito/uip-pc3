# INVESTIGACIÓN

*Fecha de Revisión*: 02/11/2015

## Instrucciones:
1. Averiguar qué es un API y cómo implementarlo en Python. Pueden tomar como referencia los siguientes enlaces:
 * http://blog.miguelgrinberg.com/post/designing-a-restful-api-with-python-and-flask
 * http://gotofritz.net/blog/weekly-challenge/restful-python-api-bottle/
 * http://myadventuresincoding.wordpress.com/2011/01/02/creating-a-rest-api-in-python-using-bottle-and-mongodb/
 * http://www.robertshady.com/content/creating-very-basic-api-using-python-django-and-piston
