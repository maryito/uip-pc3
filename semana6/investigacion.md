# INVESTIGACIÓN

*Fecha de Revisión*: 19/10/2015

## Instrucciones:
1. Leer el documento complementario/unittest.pdf acerca de las pruebas unitarias en Python:
   - Diseñar un mapa mental sobre las pruebas unitarias.
   - Escribir un programa que implemente pruebas unitarias.
